import calendar
import glob
import os
import pickle
import time
import tkinter as tk
from PIL import ExifTags, Image, ImageTk

import pycdates
import pyctools
import scrolledframe as sf

FIRSTWEEKDAY = 6
FONT_L = ("Century Schoolbook L", 20)
FONT_M = ("Century Schoolbook L", 15)
FONT_S = ("Century Schoolbook L", 12)
MONTH = {str(month).zfill(2): calendar.month_abbr[month] for month in range(1, 13)}
DAY = calendar.day_abbr

class PycMonths(tk.Frame, pyctools.PycTools):
  def __init__(self, master, stock):
    tk.Frame.__init__(self, master)
    self.displayed = None # 表示されているカレンダーウィジェット
    self.frames = [] # カレンダーウィジェットのリスト
    self.thumbnails = {}
    if "PycThumbs" in glob.glob("*"):
      self.ThumbsDB("r")
    for year in sorted(list(stock.files_dict.keys())):
      if not year in list(self.thumbnails.keys()):
        self.thumbnails[year] = {str(x).zfill(2) : {}  for x in range(1,13)}
      for month in sorted(list(stock.files_dict[year].keys())):
        self.create_month_widget(stock, year, month)
    self.init_load()

  
###################################################
  def create_month_widget(self, stock, year, month): # カレンダーのウィジェットを作成する(packはしない)
    cal = calendar.Calendar(firstweekday = FIRSTWEEKDAY)
    day_color = ["black"] * 5 + ["blue"] + ["red"] # 曜日毎の色の設定
    weeks = cal.monthdayscalendar(int(year), int(month)) # 週毎の日付のリスト
    if len(weeks) < 6: weeks += [[0, 0, 0, 0, 0, 0, 0]] * (6 - len(weeks)) # リストの長さの調節
    
    monthframe = tk.Frame(self, bd = 2, relief = tk.GROOVE)
    self.frames += [(year + ":" + month, monthframe)]
    
    monthframe.monthinfo = tk.Frame(monthframe)
    monthframe.monthinfo.grid(row = 0, sticky = tk.N + tk.S + tk.E + tk.W)
    monthframe.monthinfo.infolabel = tk.Label(monthframe.monthinfo, font = FONT_L, text = MONTH[month] + " / " + year)
    monthframe.monthinfo.infolabel.pack()
    
    monthframe.monthcal = tk.Frame(monthframe)
    monthframe.monthcal.grid(row = 1, sticky = tk.N + tk.S + tk.E + tk.W)
    
    for day in range(7):
      if FIRSTWEEKDAY == 6: day -= 1
      tk.Label(monthframe.monthcal, bd = 2, fg = day_color[day], font = FONT_M, relief = tk.RIDGE, text = DAY[day]).grid(row = 0, column = (day + 1) % 7 if FIRSTWEEKDAY == 6 else day % 7, sticky = tk.N + tk.S + tk.E + tk.W)
    
    for i, week in enumerate(weeks):
      for j, date in enumerate(week):
        cellframe = tk.Frame(monthframe.monthcal, bd = 2, relief = tk.RIDGE)
        cellframe.grid(row = i + 1, column = j % 7, sticky = tk.N + tk.S + tk.E + tk.W)
        date = str(date).zfill(2)
        if (date in list(self.thumbnails[year][month].keys()) and
            not date in list(stock.files_dict[year][month].keys())):
          del(self.thumbnails[year][month][date])
          print("Deleted thumbnails on", str(int(date)) + "/" + month + "/" + year) 

        if date in list(self.thumbnails[year][month].keys()):
          print("Loading", stock.files_dict[year][month][date][0][1])
          image = self.thumbnails[year][month][date]
          photo = ImageTk.PhotoImage(image)
          cellframe.photo = photo
          photolabel = tk.Label(cellframe, image = photo)
          photolabel.bind("<1>", lambda event, date = date: self.months2dates(stock, year, month, date))
        
        elif date in list(stock.files_dict[year][month].keys()):
          print("Generating", stock.files_dict[year][month][date][0][1])
          image = Image.open(stock.files_dict[year][month][date][0][1])
          if os.path.splitext(stock.files_dict[year][month][date][0][1])[1] in [".jpg", ".JPG"]: # JPEGならExifのOrientationタグを確認する
            image = self.RotatePic(image)
          image =  image.resize((105, 80), Image.ANTIALIAS)
#          image.thumbnail((105, 80))
          photo = ImageTk.PhotoImage(image)
          cellframe.photo = photo
          self.thumbnails[year][month][date] = image
          photolabel = tk.Label(cellframe, image = photo)
          photolabel.bind("<1>", lambda event, date = date: self.months2dates(stock, year, month, date))
        
        else:
          photolabel = tk.Label(cellframe, width = 13, height = 6)
        date = int(date)
        photolabel.grid(row = 0, column = 0, sticky = tk.N + tk.S + tk.E + tk.W)
        tk.Label(cellframe, fg = day_color[j - 1 if FIRSTWEEKDAY == 6 else j], font = FONT_S, text = date or "").grid(row = 0, column = 0, sticky = tk.NW)
    self.ThumbsDB("w")


###################################################
  def ThumbsDB(self, mode):
    if mode == "r":
      pycdb = open("PycThumbs", "rb")
      self.thumbnails = pickle.load(pycdb)
    elif mode == "w":
      pycdb = open("PycThumbs", "wb")
      pickle.dump(self.thumbnails, pycdb)



###################################################
  def init_load(self):
    today = time.strftime("%Y:%m", time.localtime())
    dates = [date[0] for date in self.frames]
    if today in dates: # 起動した日付のカレンダーがあれば表示
      index = dates.index(today)
      self.displayed = self.frames[index]
    elif self.frames: # そうでなければ最初の月のカレンダーを表示
      self.displayed = self.frames[0]
    else: # 初回起動時はself.framesになにも入っていない
      return
    self.displayed[1].pack(expand = True, padx = 20, pady = 14)
    self.displayed[1].bind_all("<4>", self.loader) # マウスウィールで前後の月を読み込む
    self.displayed[1].bind_all("<5>", self.loader) # 新しく表示した月にはわざわざbindしなくてよい
  
  
  def loader(self, event):
    if event.num == 4: # 上スクロール
      if self.displayed == self.frames[0]: return # 一番古い月ならなにもしない
      else:
        index = self.frames.index(self.displayed) # 表示されている月のself.framesにおけるインデックスを取得
        self.displayed[1].pack_forget() # 表示されている月を非表示に
        self.displayed = self.frames[index -1] # 表示されている月の前の月をself.displayedにいれる
    elif event.num == 5: # 下スクロール
      if self.displayed == self.frames[-1]: return # 一番新しい月ならなにもしない
      else:
        index = self.frames.index(self.displayed) # 表示されている月のself.framesにおけるインデックスを取得
        self.displayed[1].pack_forget() # 表示されている月を非表示に
        self.displayed = self.frames[index + 1] # 表示されている月の後の月をself.displayedにいれる
    self.displayed[1].pack(expand = True, padx = 20, pady = 14) # self.displayedにいれた月を表示する
    self.displayed[1].bind_all("<4>", self.loader) # マウスウィールで前後の月を読み込む
    self.displayed[1].bind_all("<5>", self.loader) # 新しく表示した月にはわざわざbindしなくてよい


###################################################
  def months2dates(self, stock, year, month, date):
    pycframe = stock.pycwidgets[pycdates.PycDates]
    pycframe.init_load(year, month, date)
    pycframe.canvas.yview_moveto(0)
    pycframe.ScrolledFrame.bind_all("<3>", lambda event: self.dates2months(pycframe))
    pycframe.ScrolledFrame.bind_all("<4>", pycframe.mousewheel_y)
    pycframe.ScrolledFrame.bind_all("<5>", pycframe.mousewheel_y)
    pycframe.tkraise()


  def dates2months(self, pycframe):
    self.displayed[1].bind_all("<4>", self.loader) # マウスウィールで前後の月を読み込む
    self.displayed[1].bind_all("<5>", self.loader)
    self.tkraise()
    




if __name__ == "__main__":
  root = tk.Tk()
  frame = tk.Frame(root)
  frame.files = {'2012': {'09': {}, '07': {}, '12': {}, '05': {}, '11': {}, '06': {}, '01': {}, '08': {'30': [('21:03:42', './testpics/DSCF1654.JPG'), ('21:04:10', './testpics/DSCF1655.JPG'), ('21:10:12', './testpics/DSCF1656.JPG'), ('21:10:20', './testpics/DSCF1657.JPG')], '31': [('00:09:18', './testpics/DSCF1658.JPG'), ('00:09:50', './testpics/DSCF1659.JPG'), ('00:10:40', './testpics/DSCF1660.JPG'), ('00:11:44', './testpics/DSCF1661.JPG'), ('00:12:14', './testpics/DSCF1662.JPG'), ('00:15:10', './testpics/DSCF1665.JPG'), ('00:15:20', './testpics/DSCF1666.JPG'), ('04:28:20', './testpics/DSCF1667.JPG')]}, '03': {}, '02': {}, '04': {}, '10': {}}, '2013': {'09': {}, '07': {}, '12': {}, '05': {}, '11': {}, '06': {}, '01': {}, '08': {}, '03': {}, '02': {'03': [('14:27:33', './testpics/IMG_0553.JPG')], '02': [('17:11:30', './testpics/IMG_0540.JPG'), ('17:57:55', './testpics/IMG_0541.JPG'), ('20:07:53', './testpics/IMG_0542.JPG'), ('20:08:07', './testpics/IMG_0543.JPG'), ('20:08:15', './testpics/IMG_0545.JPG'), ('20:08:19', './testpics/IMG_0546.JPG'), ('20:08:33', './testpics/IMG_0547.JPG'), ('20:08:41', './testpics/IMG_0548.JPG'), ('22:07:38', './testpics/IMG_0549.JPG'), ('23:25:04', './testpics/IMG_0550.JPG'), ('23:25:34', './testpics/IMG_0551.JPG'), ('23:29:10', './testpics/IMG_0552.JPG')]}, '04': {}, '10': {}}}
  PycMonths(root, frame).pack()
  root.mainloop()
