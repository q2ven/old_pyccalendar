import os
import tkinter as tk
from PIL import Image, ImageTk

import scrolledframe as sf
import pyctools

class PycPyc(tk.Frame, pyctools.PycTools):
  def __init__(self, master, stock):
    tk.Frame.__init__(self, master)
    

###################################################    
  def display_pic(self, stock, photoinfo): # PycDatesから呼び出された際に写真を表示する
    photo = self.image_open(stock, photoinfo)
    self.displayed = tk.Label(self, image = photo)
    self.displayed.pack(expand = True, fill = tk.BOTH)
    self.photo = photo
    self.bind_all("<4>", lambda event: self.loader(event, stock, photoinfo)) # スクロールで前後の画像を読み込む
    self.bind_all("<5>", lambda event: self.loader(event, stock, photoinfo))


  def image_open(self, stock, photoinfo): # 写真の情報を元にImageTk.PhotoImageを返す
    image = Image.open(photoinfo[1])
    size = self.getsize(stock.container.main)
    if os.path.splitext(photoinfo[1])[1] in (".jpg", ".JPG"): # resizeメソッドはEXIF情報を保持しないので先に回転させる
      image = self.RotatePic(image)
    if image.size[0] > image.size[1]:
      size = (size[0], size[0] * image.size[1] // image.size[0])
    else:
      size = (size[1] * image.size[0] // image.size[1], size[1])  
    image = image.resize(size, Image.ANTIALIAS)
    return ImageTk.PhotoImage(image)
  
  
  def loader(self, event, stock, photoinfo):
    if event.num == 4:
      if photoinfo == stock.files_list[0]: return # 最も古い画像ならなにもしない
      else: index = stock.files_list.index(photoinfo) - 1 # 今の画像の1つ前のfiles_listのインデックスを取得する
    elif event.num == 5:
      if photoinfo == stock.files_list[-1]: return # 最も新しい画像ならなにもしない
      else: index = stock.files_list.index(photoinfo) + 1 # 今の画像の1つ後のfiles_listのインデックスを取得する
    photo = self.image_open(stock, stock.files_list[index]) # 取得したインデックスにある情報を元に写真を開く
    self.displayed.pack_forget() # 表示されている画像を非表示にする
    del(self.displayed) # ウィンドウサイズに対応させるために二度目に表示する画像も最初から開き直すので一回目の画像は消去する
    self.displayed = tk.Label(self, image = photo) # 新たな画像を表示する
    self.displayed.pack(expand = True, fill = tk.BOTH)
    self.photo = photo
    self.bind_all("<4>", lambda event: self.loader(event, stock, stock.files_list[index])) # 新たにbindしないとloaderに渡すphotoinfoが更新されない
    self.bind_all("<5>", lambda event: self.loader(event, stock, stock.files_list[index]))

