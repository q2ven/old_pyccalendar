import calendar
import os
import tkinter as tk
from PIL import Image, ImageTk

import scrolledframe as sf
import pycmonths
import pycpyc
import pyctools

FONT_L = ("Century Schoolbook L", 16)
MONTH = {str(month).zfill(2): calendar.month_abbr[month] for month in range(1, 13)}
DATE = {"%02d" % x : str(x) if len(str(x)) > 1 else " " + str(x) for x in range(1, 32)}

class PycDates(sf.ScrolledFrame, pyctools.PycTools):
  def __init__(self, master, stock):
    sf.ScrolledFrame.__init__(self, master, X = 1, Y = 1)
    self.picdata = {} # 写真の情報と写真のデータとそのラベルのタプルが入った辞書
    self.dateframes_dict = {} # 年月日をキーにした日付ごとのフレームの辞書
    self.dateframes_list = []
    self.dates = [] # 写真の存在する日付のリスト
    self.displayed = []
    self.created_photo = []
    self.stock = stock
    for year in sorted(list(stock.files_dict.keys())):
      for month in sorted(list(stock.files_dict[year].keys())):
        for date in sorted(list(stock.files_dict[year][month].keys())):
          self.dates += [":".join([year, month, date])]
    
          
###################################################
  def create_date_widget(self, stock, key): # 日付ごとのフレームを返す
    year, month, date = key.split(":")
    if key in list(self.dateframes_dict.keys()): # すでに作られた日付のフレームがあればパックして返す
      dateframe = self.dateframes_dict[key]
      dateframe.pack(expand = True, fill = tk.BOTH, ipadx = 3, pady = 15)
      print("Already Made", key)
      return dateframe
    else: # まだ日付のフレームが作られていないのであれば作成する
      dateframe = tk.Frame(self.ScrolledFrame) # 日付毎のフレーム
      dateframe.pack(expand = True, fill = tk.BOTH, ipadx = 3, pady = 15)
      self.dateframes_dict[key] = dateframe # 辞書に日付をキーにして登録する
      self.dateframes_list += [(key, dateframe)]
      #self.dateframes_list.sort() # 日付順にソート
      print("Made", key)
      
      dateframe.dateinfo = tk.Frame(dateframe) # その日付の情報
      dateframe.dateinfo.grid(row = 0, stick = tk.NW)
      dateframe.dateinfo.infolabel = tk.Label(dateframe.dateinfo, text = "  " + DATE[date] + " " + MONTH[month] + "   " + year, font = FONT_L)
      dateframe.dateinfo.infolabel.pack(side = tk.LEFT)
    
      dateframe.datephotos = tk.Frame(dateframe) # その日付の写真が収まるフレーム
      dateframe.datephotos.grid(padx = 5, row = 1, stick = tk.NW)
          
      if not year in self.picdata: # もし日付毎のリストが作られていなければリストを作成
        self.picdata[year] = {str(x).zfill(2) : {}  for x in range(1,13)}
      if not date in self.picdata[year][month]:
        self.picdata[year][month][date] = []
      for i, photoinfo in enumerate(stock.files_dict[year][month][date]):
        if not photoinfo in self.created_photo: # すでに写真が表示されているかを確認
          self.created_photo += [photoinfo] # すでに作った写真の情報をリストに追加する
          
          #print("Loading", photoinfo[1], "...")
        
          image = Image.open(photoinfo[1]) # 写真の読み込み
          size = (image.size[0] * 80 // image.size[1], 80)
          image.thumbnail(size) # thumbnailメソッドはEXIF情報を保持する
          if os.path.splitext(photoinfo[1])[1] in (".jpg", ".JPG"): # JPEGなら回転処理等を施す
            image = self.RotatePic(image)
          photo = ImageTk.PhotoImage(image)
          photolabel = tk.Label(dateframe.datephotos, image = photo)
          photolabel.grid(ipadx = 4, row = i // 7, column = i % 7)
          photolabel.bind("<Double-Button-1>", lambda event, photoinfo = photoinfo: self.dates2pyc(stock, photoinfo))
          self.picdata[year][month][date] += [photoinfo + (photo, photolabel)] # 表示した写真のデータを保持する
          self.picdata[year][month][date].sort() # 時系列に並び替える
          
          #print("Complete Loading", photoinfo[1])
      return dateframe


#############################################
  def init_load(self, year, month, date): # PycMonthsからPycDatesに移る際の前処理
    for frame in self.displayed: # すでに表示されているフレームをすべて非表示にする
      frame.pack_forget()
    self.displayed = [] # 表示したフレームをいれるリストを空にする
    key = ":".join([year, month, date])
    index = self.dates.index(key) # クリックされた日付が写真が登録された日付のリストの中で何番目に位置するかを取得
    self.prevdate = index # 表示される先頭の日付のself.datesにおけるインデックスを保持
    if self.dates.index(key) + 4 < len(self.dates): # もし最後から5番目よりも前なら5日分を表示する
      for x in range(5):
        key = self.dates[index + x]
        self.displayed += [self.create_date_widget(self.stock, key)] # 5日分のフレームを表示するリストに入れる
      self.nextdate = index + x # 表示される最後の日付のself.datesにおけるインデックスを保持
    else: # 最後から5番目以内なら最後の日付分まで表示する
      num = len(self.dates) - self.dates.index(key)
      for x in range(num):
        key = self.dates[index + x]
        self.displayed += [self.create_date_widget(self.stock, key)]
      self.nextdate = 0 # 0の場合は次のフレームを表示しないようにloader関数で定める


  def loader(self, mode): # PycDates中のスクロールで前後の日付を読み込む
    if mode == 4: # 上スクロール
      if self.prevdate == 0: return # 先頭の日付よりも前の写真がない場合は何もしない
      self.prevdate -= 1 # 先頭の日付より前に写真がある場合は先頭の日付のself.datesにおけるインデックスを−１する
      self.displayed.insert(0, self.create_date_widget(self.stock, self.dates[self.prevdate])) # 表示されるフレームのリストの先頭に新たなフレームを挿入する
      for frame in self.displayed: # すべての表示されたフレームを一度消す
        frame.pack_forget()
      for frame in self.displayed: # すべての表示されるフレームをパックする
        frame.pack(expand = True, fill = tk.BOTH, ipadx = 3, pady = 15)
    elif mode == 5: # 下スクロール
      if self.nextdate == 0: return # 最後の日付よりも後に写真がない場合は何もしない
      if self.nextdate == len(self.dates): # 最後の日付がちょうどself.datesの最後のインデックスと一致したら次から読み込まないように0をいれる
        self.nextdate = 0
        return
      self.displayed.insert(-1, self.create_date_widget(self.stock, self.dates[self.nextdate])) # 表示されるフレームのリストの催行日に新たなフレームを挿入する
      self.nextdate += 1



  def mousewheel_y(self, event):
    if event.num == 4:
      self.canvas.yview(*self.scroll_opt["4"])
      if self.vscrollbar.get()[0] == 0: # スクロールして一番上にいったら前の日のフレームを読み込む
        self.loader(event.num)
    elif event.num == 5:
      self.canvas.yview(*self.scroll_opt["5"])
      if self.vscrollbar.get()[1] == 1: # スクロールして一番下にいったら後の日のフレームを読み込む
        self.loader(event.num)


#############################################
  def dates2pyc(self, stock, photoinfo):
    pycframe = stock.pycwidgets[pycpyc.PycPyc]
    pycframe.display_pic(stock, photoinfo)
    pycframe.bind_all("<3>", lambda event: self.pyc2dates(stock, pycframe))
    pycframe.tkraise()
    
  def pyc2dates(self, stock, pycframe):
    pycframe.displayed.pack_forget()
    pycframe = stock.pycwidgets[pycmonths.PycMonths]
    self.bind_all("<3>", lambda event: pycframe.dates2months(self))
    self.bind_all("<4>", self.mousewheel_y)
    self.bind_all("<5>", self.mousewheel_y)
    self.tkraise()
    


if __name__ == "__main__":  
  def ResizeWidget(widget):
    size = widget.master.winfo_width(), widget.master.winfo_height()
    widget.config(width = size[0], height = size[1])

  root = tk.Tk()
  frame = tk.Frame(root)
  frame.pack()
  frame.files = {'2012': {'09': {}, '07': {}, '12': {}, '05': {}, '11': {}, '06': {}, '01': {}, '08': {'30': [('21:03:42', './testpics/DSCF1654.JPG'), ('21:04:10', './testpics/DSCF1655.JPG'), ('21:10:12', './testpics/DSCF1656.JPG'), ('21:10:20', './testpics/DSCF1657.JPG')], '31': [('00:09:18', './testpics/DSCF1658.JPG'), ('00:09:50', './testpics/DSCF1659.JPG'), ('00:10:40', './testpics/DSCF1660.JPG'), ('00:11:44', './testpics/DSCF1661.JPG'), ('00:12:14', './testpics/DSCF1662.JPG'), ('00:15:10', './testpics/DSCF1665.JPG'), ('00:15:20', './testpics/DSCF1666.JPG'), ('04:28:20', './testpics/DSCF1667.JPG')]}, '03': {}, '02': {}, '04': {}, '10': {}}, '2013': {'09': {}, '07': {}, '12': {}, '05': {}, '11': {}, '06': {}, '01': {}, '08': {}, '03': {}, '02': {'03': [('14:27:33', './testpics/IMG_0553.JPG')], '02': [('17:11:30', './testpics/IMG_0540.JPG'), ('17:57:55', './testpics/IMG_0541.JPG'), ('20:07:53', './testpics/IMG_0542.JPG'), ('20:08:07', './testpics/IMG_0543.JPG'), ('20:08:15', './testpics/IMG_0545.JPG'), ('20:08:19', './testpics/IMG_0546.JPG'), ('20:08:33', './testpics/IMG_0547.JPG'), ('20:08:41', './testpics/IMG_0548.JPG'), ('22:07:38', './testpics/IMG_0549.JPG'), ('23:25:04', './testpics/IMG_0550.JPG'), ('23:25:34', './testpics/IMG_0551.JPG'), ('23:29:10', './testpics/IMG_0552.JPG')]}, '04': {}, '10': {}}}

  sf = PycDates(frame, frame)
  sf.pack()
  root.bind("<Configure>", lambda event: ResizeWidget(frame))
  root.mainloop()

