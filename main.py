#! /usr/bin/env python3.4

import glob
import os
import pickle
import stat
import time
import tkinter as tk
from PIL import ExifTags, Image, ImageTk
from tkinter import filedialog as fd

import pycdates
import pycmonths
import pycpyc
import pyctools

EXTENSIONS = [".jpg", ".JPG", ".png", ".PNG"]
FILETYPES = [("JPG", ".jpg"), ("JPG", ".JPG"), ("PNG", ".png"), ("PNG", ".PNG")]


class PycCalendar(tk.Tk):
  def __init__(self):
    tk.Tk.__init__(self)
    self.title("PycCalendar")
    self.minsize(1030, 640)
    
    self.file_opt = options = {} # ファイル等を開く際のオプション
    options["defaultextension"] = ".jpg"
    options["filetypes"] = FILETYPES
    options["initialdir"] = "./"
    options["parent"] = self
    options["title"] = "Open a file"
    
    self.dir_opt = options = {}
    options["initialdir"] = "./"
    options["mustexist"] = False
    options["parent"] = self
    options["title"] = "Open a directory"
    
    self.files_dict = {}
    self.files_list = []
    if "PycDB" in glob.glob("*"):
      self.pycdb("r")
    self.pycwidgets = {}
    
    self.create_menu()
    self.create_frames()
    self.create_pycwidgets()

    
  def getsize(self, widget, types = None):
    if types == None:
      return widget.winfo_width(), widget.winfo_height()
    if types == dir:
      opt = {}
      opt["width"] = widget.winfo_width()
      opt["height"] = widget.winfo_height()
      return opt
      
################################################### 
  def create_menu(self): # メニューバーの作成
    self.menu = tk.Menu(self) # メニューバーの大枠を作る
    self.config(menu = self.menu)
    self.menu.files = tk.Menu(self.menu, tearoff = False) # メニューを作成
    self.menu.edit = tk.Menu(self.menu, tearoff = False)
    
    self.menu.add_cascade(label = "Files", under = 0, menu = self.menu.files) # メニューを大枠に結びつける
    self.menu.add_cascade(label = "Edit", under = 0, menu = self.menu.edit)

    self.menu.files.add_command(label = "Open a file", under = 0, command = self.askopenfilename) # メニュー内の項目の作成
    self.menu.files.add_command(label = "Open a directory", under = 0, command = self.askdirectory)
    self.menu.files.add_command(label = "Quit", under = 0, command = self.destroy)

    
  def askopenfilename(self): # 1つのファイルを開く
    filepath = fd.askopenfilename(**self.file_opt)
    if filepath and not filepath in self.files:
      self.file_opt["initialdir"] = self.dir_opt["initialdir"] = os.path.dirname(filepath) # 空でなければ次は同じディレクトリから開く
      self.register_pics(filepath)
      self.pycdb("w")

  
  def askdirectory(self):
    dirpath = fd.askdirectory(**self.dir_opt)
    if dirpath: 
      self.file_opt["initialdir"] = self.dir_opt["initialdir"] = dirpath # 空でなければ次は同じディレクトリから開く
      filepaths = []
      self.dirrecursion(filepaths, dirpath)
      for filepath in filepaths:
        self.register_pics(filepath)
      self.pycdb("w")


  def dirrecursion(self, filepaths, dirname): # ディレクトリを再帰的に読み込み、すべてのファイルパスを返す
    contents = glob.glob(dirname + "/*")
    for content in contents:
      status = os.stat(content)
      if stat.S_ISDIR(status.st_mode):
        self.dirrecursion(filepaths, content)
      else:
        if os.path.splitext(content)[1] in EXTENSIONS:
          filepaths += [content]

  
  def register_pics(self, path): # 写真を辞書とリストに収める
    if os.path.splitext(path)[1] in EXTENSIONS[:2]: # JPEGならEXIF情報をみる
      image = Image.open(path)
      if image._getexif(): # EXIF情報のないJPEGではNoneが返されitemsメソッドが使えないため
        exif = {ExifTags.TAGS[key]: item for key, item in image._getexif().items() if key in ExifTags.TAGS}
      else:
        exif = {}
      keys = list(exif.keys())
      if "DateTimeOriginal" in keys:
        datetime = exif["DateTimeOriginal"]
      elif "DateTimeDigitized" in keys:
        datetime = exif["DateTimeDigitized"]
      elif "DateTime" in keys:
        datetime = exif["DateTime"]
      else: # EXIF情報が読み取れなければstatで生成日時を取得
        status = os.stat(path)
        datetime = time.strftime("%Y:%m:%d %H:%M:%S", time.localtime(status[stat.ST_MTIME]))
    else: # JPEG以外はstatで生成日時を読み込む
      status = os.stat(path)
      datetime = time.strftime("%Y:%m:%d %H:%M:%S", time.localtime(status[stat.ST_MTIME]))
    
    year, month, date = datetime.split()[0].split(":") # 年月日を取得する
    if not year in self.files_dict: # もし年の辞書がなければ一年分の辞書を作る
      self.files_dict[year] = {str(x).zfill(2): {} for x in range(1, 13)}
    if not date in self.files_dict[year][month]: # もし日付のリストがなければ作る
      self.files_dict[year][month][date] = []
    if not (datetime, path) in self.files_dict[year][month][date]: # すでに登録されている場合は登録しない
      self.files_dict[year][month][date] += [(datetime, path)] # 辞書に登録
      self.files_list += [(datetime, path)] # リストに登録
      self.files_dict[year][month][date].sort() # 時系列でソートする
      self.files_list.sort()
      print("Loading", path)


###################################################
  def pycdb(self, mode): # データベース
    if mode == "r": # データベースを読み込む
      pycdb = open("PycDB", "rb")
      self.files_dict, self.files_list = pickle.load(pycdb)
      pycdb.close()
    elif mode == "w": # データベースに書き込む
      pycdb = open("PycDB", "wb")
      pickle.dump((self.files_dict, self.files_list), pycdb)
      pycdb.close()

###################################################
  def create_frames(self): # 基盤となるフレームを作成する
    self.container = container = tk.Frame(self)
    container.pack(expand = True, fill = tk.BOTH) # subを先にpackしないとresize_framesの挙動がおかしくなる
    container.sub = tk.Frame(container, bg = "yellow")
    container.sub.pack(expand = True, fill = tk.BOTH, side = tk.LEFT)
    container.main = tk.Frame(container)
    container.main.pack(expand = True, fill = tk.BOTH, side = tk.RIGHT)
    self.bind("<Configure>", self.resize_frames)
  
  def resize_frames(self, event): # subフレームのサイズを一定に保つ
    size = self.getsize(self.container)
    self.container.sub.config(width = 200, height = size[1])


###################################################
  def create_pycwidgets(self): # Pycフレームを作っていく
    for pycwidget in [pycpyc.PycPyc, pycdates.PycDates, pycmonths.PycMonths]:
      self.pycwidgets[pycwidget] = pycwidget(self.container.main, self)
      self.pycwidgets[pycwidget].grid(row = 0, column = 0, sticky = tk.N + tk.S + tk.E + tk.W)


if __name__ == "__main__":
  PycCalendar().mainloop()
