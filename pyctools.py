from PIL import ExifTags, Image

class PycTools(): # 各モジュールに共通の関数を収納

  def getsize(self, widget, types = None): # ウィジェットのサイズをリストか辞書で返す
    if types == None:
      return widget.winfo_width(), widget.winfo_height()
    if types == dir:
      opt = {}
      opt["width"] = widget.winfo_width()
      opt["height"] = widget.winfo_height()
      return opt

  def RotatePic(self, image): # Orientation情報が含まれていればその通りに回転、反転する
    if image._getexif(): # EXIF情報のないJPEGではNoneが返されitemsメソッドが使えないため
      exif = {ExifTags.TAGS[key]: item for key, item in image._getexif().items() if key in ExifTags.TAGS}
    else:
      exif = {}
    if "Orientation" in list(exif.keys()):
      orientation = exif["Orientation"]
      if orientation == 1:
        return image
      elif orientation == 2:
        return image.transpose(Image.FLIP_LEFT_RIGHT)
      elif orientation == 3:
        return image.transpose(Image.ROTATE_180)
      elif orientation == 4:
        return image.transpose(Image.FLIP_TOP_BOTTOM)
      elif orientation == 5:
        return image.transpose(Image.FLIP_TOP_BOTTOM).transpose(Image.ROTATE_90)
      elif orientation == 6:
        return image.transpose(Image.ROTATE_270)
      elif orientation == 7:
        return image.transpose(Image.FLIP_TOP_BOTTOM).transpose(Image.ROTATE_270)
      elif orientation == 8:
        return image.transpose(Image.ROTATE_90)
    else:
      return image
