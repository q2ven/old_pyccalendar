import tkinter as tk

class ScrolledFrame(tk.Frame):
  def __init__(self, master, autoconfig = True, X = False, Y = True, scrollbind = True, scroll_opt = ["scroll", -1, "units"]):
    """
    フレーム(self)の中にキャンバスを入れて
    そこにスクロールバーをつけて
    フレーム(ScrolledFrame)を埋め込む
    注意 ::　ウィジェットの作成順が重要なので下部の条件分岐をまとめられない
    説明 :: 
        autoconfig がTrueだと親ウィジェットに合わせてサイズを変え、子ウィジェットに合わせてスクロール幅を変える
    　　　　X が水平軸のスクロールバーをつけるかどうか(True/False)
    　　　　Y　が垂直軸のスクロールバーをつけるかどうか(True/False) FalseでもscrollbindがTrueならスクロールは可能
    　　　　scrollbind がTrueになるとcanvas上のどこでスクロールしても垂直(Y)軸のスクロールが実行される
    　　　　scroll_opt でscrollの仕方を決める(canvas.yview/canvas.xviewに渡すオプション)
    """
    
    tk.Frame.__init__(self, master)
    self.scroll_opt = option = {}
    if scroll_opt[0] == "scroll":
      if scroll_opt[1] > 0:
        option["5"] = scroll_opt[:] # コピーしないと次の行で上書きされて片方のスクロールができなくなる
        scroll_opt[1] *= -1
        option["4"] = scroll_opt
      else:
        option["4"] = scroll_opt[:] # コピーしないと次の行で上書きされて片方のスクロールができなくなる
        scroll_opt[1] *= -1
        option["5"] = scroll_opt
    elif scroll_opt[0] == "moveto":
        self.scroll_opt = scroll_opt
    
    if Y:
      self.vscrollbar = tk.Scrollbar(self, orient = "v")
      self.vscrollbar.pack(expand = False, fill = tk.Y, side = tk.RIGHT)
      
    if X:
      self.hscrollbar = tk.Scrollbar(self, orient = "h")
      self.hscrollbar.pack(expand = False, fill = tk.X, side = tk.BOTTOM)
      
    self.canvas = tk.Canvas(self)
    self.canvas.pack(expand = True, fill = tk.BOTH, side = tk.LEFT)
      
    if Y:
      self.vscrollbar.config(command = self.canvas.yview)
      self.canvas.config(yscrollcommand = self.vscrollbar.set)
    if X:
      self.hscrollbar.config(command = self.canvas.xview)
      self.canvas.config(xscrollcommand = self.hscrollbar.set)
      
    
    self.ScrolledFrame = tk.Frame(self.canvas)
    self.ScrolledFrame.pack(expand = True, fill = tk.BOTH)
    self.canvas.create_window(0, 0, anchor = tk.NW, window = self.ScrolledFrame)
    
    if autoconfig:
      self.bind_all("<Configure>", self.resize_frames)
    
    if scrollbind:
      self.canvas.bind_all("<4>", self.mousewheel_y)
      self.canvas.bind_all("<5>", self.mousewheel_y)


  def getsize(self, widget, types = None, req = None):
    if req:
      if types in (None, list):
        return widget.winfo_reqwidth(), widget.winfo_reqheight()
    else:
      if types in (None, list):
        return widget.winfo_width(), widget.winfo_height()
      if types == dir:
        opt = {}
        opt["width"] = widget.winfo_width()
        opt["height"] = widget.winfo_height()
        return opt
    
  

  def resize_frames(self, event):
    """
    フレーム(self)の親ウィジェットのサイズにキャンバスを拡大して
    キャンバス内のフレーム(ScrolledFrame)の幅にスクロールの幅を合わせる
    """
    size = self.getsize(self.master, dir)
    self.config(**size)
    size = self.getsize(self.ScrolledFrame)
    self.canvas.config(scrollregion = "0 0 %s %s" % size)
    

  def mousewheel_y(self, event):
    if event.num == 4:
      self.canvas.yview(*self.scroll_opt["4"])
    elif event.num == 5:
      self.canvas.yview(*self.scroll_opt["5"])

  

if __name__ == "__main__":
  """
  フレームの中にScrolledFrameを入れる場合は
  親のフレームのサイズを変更するように設定しないと
  ScrolledFrameのサイズも変わらない
  逆にいうと親フレームのサイズが変わると自動で
  ScrolledFrameのサイズも変わる
  """
  def ResizeWidget(widget):
    size = widget.master.winfo_width(), widget.master.winfo_height()
    widget.config(width = size[0], height = size[1])

  root = tk.Tk()
  frame = tk.Frame(root)
  frame.pack()
  sf = ScrolledFrame(frame)
  sf.pack()
  root.bind("<Configure>", lambda event: ResizeWidget(frame))
  root.mainloop()
